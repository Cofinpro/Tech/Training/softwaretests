package de.cofinpro.training.softwaretests.exercise.unittest;

import de.cofinpro.training.softwaretests.exercise.unittest.dao.UuidDao;
import de.cofinpro.training.softwaretests.exercise.unittest.loader.Loader;
import de.cofinpro.training.softwaretests.exercise.unittest.loader.resource.NumbersResource;
import de.cofinpro.training.softwaretests.exercise.unittest.loader.resource.NumbersResourceFactory;
import de.cofinpro.training.softwaretests.exercise.unittest.loader.resource.Resource;
import de.cofinpro.training.softwaretests.exercise.unittest.service.ServiceClient;
import de.cofinpro.training.softwaretests.exercise.unittest.service.ServiceObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Diese Klasse ist die Basis einer Übung, die sich um JUnit und Mockito dreht.
 * Auf relativ umständliche Art und Weise werden in der Methode {@link #persistUuids()} folgende Schritte durchgeführt:
 * <p>
 * - Mithilfe einer sog. Ressource wird ein Ladevorgang durchgeführt.
 * Das Pattern des Ladevorgangs sieht so aus, dass in eine Map als Key eine Ressource ({@link NumbersResource})
 * hinzugefügt und als value "NULL" existiert.
 * Anschließend ändert der {@link Loader} diese Map, indem der Wert, der der Ressource zugewiesen ist,
 * nicht mehr "NULL" ist, sondern das Resultat des Ladevorgangs beinhaltet. Der Key bleibt weiterhin bestehen.
 * - Der geladene Wert stellt ein Integer-Array dar, welches numerische IDs enthält. Diese IDs werden für eine Abfrage
 * eines Pseudo-Services mithilfe des {@link ServiceClient}s verwendet, welcher mit eine Menge an Service-Objekten antwortet.
 * - Die Service-Objekte ({@link ServiceObject}) enthalten allesamt eine "UUID" ({@link UUID})
 * (siehe <a href="https://de.wikipedia.org/wiki/Universally_Unique_Identifier">https://de.wikipedia.org/wiki/Universally_Unique_Identifier</a>).
 * Diese UUIDs werden gesammelt und mithilfe des {@link UuidDao}s persistiert.
 */
public class LoaderExercise {

    private final Loader loader;
    private final ServiceClient serviceClient;
    private final UuidDao uuidDao;
    private final NumbersResourceFactory numbersResourceFactory;

    public LoaderExercise(Loader loader, ServiceClient serviceClient, UuidDao uuidDao, NumbersResourceFactory numbersResourceFactory) {
        this.loader = loader;
        this.serviceClient = serviceClient;
        this.uuidDao = uuidDao;
        this.numbersResourceFactory = numbersResourceFactory;
    }

    // Sub-Exercise: 1
    public NumbersResource getNumbersResource() {
        return numbersResourceFactory.create();
    }

    // Sub-Exercise: 2
    public int[] getIds(NumbersResource numbersResource) {
        Map<Resource, Object> resources = new HashMap<>();
        resources.put(numbersResource, null);
        loader.load(resources);
        return (int[]) resources.get(numbersResource);
    }

    // Sub-Exercise: 3
    public void storeUuids(int[] ids) {
        List<UUID> uuids = Arrays.stream(ids)
                .mapToObj(serviceClient::getObject)
                .map(ServiceObject::getUuid)
                .collect(Collectors.toList());
        uuidDao.storeUuids(uuids);
    }

    // Putting everything together
    public void persistUuids() {
        NumbersResource numbersResource = getNumbersResource();
        int[] ids = getIds(numbersResource);
        storeUuids(ids);
    }
}
