package de.cofinpro.training.softwaretests.exercise.unittest.loader;

import de.cofinpro.training.softwaretests.exercise.unittest.loader.resource.NumbersResource;
import de.cofinpro.training.softwaretests.exercise.unittest.loader.resource.Resource;

import java.util.Map;
import java.util.stream.IntStream;

public class Loader {
    public void load(Map<Resource, Object> resources) {
        if (resources != null) {
            resources.keySet().forEach(resource -> loadByResource(resource, resources));
        }
    }

    private void loadByResource(Resource resource, Map<Resource, Object> target) {
        if (resource instanceof NumbersResource) {
            NumbersResource numbersResource = (NumbersResource) resource;
            int start = numbersResource.getStart();
            int amount = numbersResource.getAmount();
            target.put(resource, IntStream.rangeClosed(start, start + amount).toArray());
        }
    }
}
