package de.cofinpro.training.softwaretests.exercise.unittest.util;

import de.cofinpro.training.softwaretests.exercise.unittest.model.UuidEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.UUID;

public class UuidPersister {
    private static final Logger LOG = LoggerFactory.getLogger(UuidPersister.class);

    public static long persistUuids(Collection<UUID> uuids, EntityManager entityManager) {
        LOG.info("Persisting UUIDs: {} ...", uuids);
        uuids.stream().map(UuidPersister::toUuidEntity).forEach(entityManager::persist);
        return System.currentTimeMillis();
    }

    private static UuidEntity toUuidEntity(UUID uuid) {
        UuidEntity entity = new UuidEntity();
        entity.setUuid(uuid.toString());
        return entity;
    }
}
