package de.cofinpro.training.softwaretests.exercise.unittest.loader.resource;

import java.util.Objects;

public class NumbersResource extends Resource {
    private final int amount;
    private final int start;

    public NumbersResource(int start, int amount) {
        this.amount = amount;
        this.start = start;
    }

    public int getAmount() {
        return amount;
    }

    public int getStart() {
        return start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumbersResource that = (NumbersResource) o;
        return amount == that.amount &&
                start == that.start;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, start);
    }
}
