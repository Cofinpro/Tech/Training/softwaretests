package de.cofinpro.training.softwaretests.exercise.unittest.config;

public class ConfigurationProvider {
    public int getStart() {
        return Integer.parseInt(System.getProperty("loaderexercise.start"));
    }

    public int getAmount() {
        return Integer.parseInt(System.getProperty("loaderexercise.amount"));
    }
}
