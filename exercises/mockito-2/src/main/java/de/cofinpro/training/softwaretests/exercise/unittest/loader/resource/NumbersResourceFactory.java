package de.cofinpro.training.softwaretests.exercise.unittest.loader.resource;

import de.cofinpro.training.softwaretests.exercise.unittest.config.ConfigurationProvider;

public class NumbersResourceFactory {
    private ConfigurationProvider configurationProvider;

    public NumbersResourceFactory(ConfigurationProvider configurationProvider) {
        this.configurationProvider = configurationProvider;
    }

    public NumbersResource create() {
        return new NumbersResource(configurationProvider.getStart(), configurationProvider.getAmount());
    }
}
