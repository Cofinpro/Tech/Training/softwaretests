package de.cofinpro.training.softwaretests.exercise.unittest.dao;

import de.cofinpro.training.softwaretests.exercise.unittest.util.UuidPersister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.UUID;

public class UuidDao {
    private static final Logger LOG = LoggerFactory.getLogger(UuidDao.class);

    private EntityManager entityManager;

    public UuidDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public long storeUuids(Collection<UUID> uuids) {
        if (uuids != null && uuids.size() < 3) {
            long timeMillis = UuidPersister.persistUuids(uuids, entityManager);
            LOG.info("Persisted following uuids: {}", uuids);
            return timeMillis;
        } else {
            throw new IllegalArgumentException("Cannot store more than 3 UUIDs at once");
        }
    }
}
