package de.cofinpro.training.softwaretests.exercise.unittest.service;

public final class ServiceClient {
    public ServiceObject getObject(int id) {
        return new ServiceObject(id);
    }
}
