package de.cofinpro.training.softwaretests.exercise.unittest.service;

import java.math.BigInteger;
import java.util.Objects;
import java.util.UUID;

public class ServiceObject {
    private final int id;
    private final UUID uuid;

    ServiceObject(int id) {
        this.id = id;
        this.uuid = UUID.randomUUID();
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceObject that = (ServiceObject) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
