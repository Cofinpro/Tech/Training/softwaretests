package de.cofinpro.training.softwaretest.mockito;

import java.util.Objects;

/**
 * @author DKostrzycki
 */
public class User {
    private String login;
    private String firstname;
    private String lastname;

    public User(String firstname, String lastname) {
        this.login = firstname.substring(0,1) + lastname;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}
