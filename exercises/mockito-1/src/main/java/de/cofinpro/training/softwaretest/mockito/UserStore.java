package de.cofinpro.training.softwaretest.mockito;

import java.util.*;

/**
 * @author DKostrzycki
 */
public class UserStore {
    private Map<String, User> users = new HashMap<>();
    private Set<String> logins = new HashSet<>();

    public void addUser(User user) {
      this.users.putIfAbsent(user.getLogin(), user);
      this.logins.add(user.getLogin());
    }

    public void removeUser(User user) {
        this.users.remove(user.getLogin());
        this.logins.remove(user.getLogin());
    }
}
