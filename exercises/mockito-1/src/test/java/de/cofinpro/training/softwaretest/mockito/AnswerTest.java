package de.cofinpro.training.softwaretest.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author DKostrzycki
 */
@ExtendWith(MockitoExtension.class)
public class AnswerTest {

    @InjectMocks
    private UserStore userStore;

    @Mock
    private Map<String, User> users;


    @Test
    public void AnswerTest() {
        final List<User> usersList = new ArrayList();
        when(users.putIfAbsent(any(String.class), any(User.class))).thenAnswer(i -> {
            final String login = i.getArgument(0);
            final User user = i.getArgument(1);
            usersList.add(user);
            return user;
        });

        userStore.addUser(new User("Hans", "Meiser"));
        userStore.addUser(new User("Otto", "Walkes"));


        assertThat(usersList).hasSize(2);
    }
}
