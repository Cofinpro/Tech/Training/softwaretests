package de.cofinpro.training.softwaretest.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author DKostrzycki
 */
@ExtendWith(MockitoExtension.class)
public class StubbingTest {

    @Mock
    private List<String> list;

    @Test
    public void stubbingTest() {
        when(list.get(1)).thenReturn("Hello");
        when(list.size()).thenReturn(2);

        assertThat(list.get(1)).isEqualTo("Hello");
        assertThat(list.size()).isEqualTo(2);

    }
}
