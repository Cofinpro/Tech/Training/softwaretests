package de.cofinpro.training.softwaretest.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

/**
 * @author DKostrzycki
 */
@ExtendWith(MockitoExtension.class)
public class SpyTest {

    @InjectMocks
    private UserStore userStore;

    @Spy
    private Set<String> logins = new HashSet<>();

    @Test
    public void spyTest() {
        final User user = new User("Dawid", "Kostrzycki");
        userStore.addUser(user);
        assertThat(logins).isNotEmpty();
        assertThat(logins).contains(user.getLogin());
    }
}
