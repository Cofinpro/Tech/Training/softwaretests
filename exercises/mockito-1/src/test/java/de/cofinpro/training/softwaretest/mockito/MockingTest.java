package de.cofinpro.training.softwaretest.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

/**
 * @author DKostrzycki
 */
@ExtendWith(MockitoExtension.class)
public class MockingTest {

    @InjectMocks
    private UserStore userStore;

    @Mock
    private Map<String, User> users;

    @Test
    public void mockingTest() {
        final User user = new User("Dawid", "Kostrzycki");
        userStore.addUser(user);
        verify(users).putIfAbsent(eq("DKostrzycki"), eq(user));
    }
}
