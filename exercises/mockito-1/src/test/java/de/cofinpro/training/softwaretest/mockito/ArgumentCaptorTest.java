package de.cofinpro.training.softwaretest.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

/**
 * @author DKostrzycki
 */
@ExtendWith(MockitoExtension.class)
public class ArgumentCaptorTest {

    @InjectMocks
    private UserStore userStore;

    @Mock
    private Map<String, User> users;

    @Captor
    private ArgumentCaptor<String> captor;

    @Test
    public void captureTest() {
        final User user = new User("Dawid", "Kostrzycki");
        userStore.addUser(user);
        verify(users).putIfAbsent(captor.capture(), eq(user));
        assertThat(captor.getValue()).isEqualTo("DKostrzycki");
    }
}
