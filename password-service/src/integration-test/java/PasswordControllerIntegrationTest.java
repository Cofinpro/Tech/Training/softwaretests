import de.cofinpro.softwaretest.password.controller.dto.NameDto;
import de.cofinpro.softwaretest.password.controller.dto.UserDto;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapper;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.junit.Ignore;
import org.junit.jupiter.api.*;

import java.util.UUID;

import static io.restassured.RestAssured.when;
import static io.restassured.RestAssured.with;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DKostrzycki
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PasswordControllerIntegrationTest {

    private static String LOGIN;

    @BeforeAll
    public static void createUserLogin() {
        LOGIN = UUID.randomUUID().toString();
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    @Order(1)
    public void isRunning() {
        RestAssured.registerParser("text/plain", Parser.TEXT);

        Response response = when().
                get("/password-service/users/is-running")
                .then()
                .statusCode(200).extract().response();
        assertEquals("ok", response.asString());
    }

    @Test
    @Order(2)
    public void createUserTest() {
        RestAssured.registerParser("text/plain", Parser.TEXT);

        final Response response =
                with()
                        .body(new UserDto(LOGIN, "TestFirstname", "TestLastname", false), ObjectMapperType.JSONB)
                        .contentType(ContentType.JSON)
                        .when()
                        .post("/password-service/users")
                        .then()
                        .statusCode(200).extract().response();
        assertEquals(LOGIN, response.asString());
    }

    @Test
    @Order(3)
    public void getUserTest() {

        when().get("/password-service/users/" + LOGIN)
                .then()
                .statusCode(200)
                .body("login", equalTo(LOGIN))
                .body("defaultPassword",equalTo(true))
                .body("firstname", equalTo("TestFirstname"))
                .body("lastname", equalTo("TestLastname"));
    }

    @Test
    @Order(4)
    public void ChangeUserNameTest() {
        with()
                .body(new NameDto("Markus", "Müller"), ObjectMapperType.JSONB)
                .contentType(ContentType.JSON)
                .when()
                .put("/password-service/users/{login}/name",LOGIN)
                .then()
                .statusCode(204);
    }
    @Test
    @Order(5)
    public void getUserAfterChangedNameTest() {

        when().get("/password-service/users/" + LOGIN)
                .then()
                .statusCode(200)
                .body("login", equalTo(LOGIN))
                .body("defaultPassword",equalTo(true))
                .body("firstname", equalTo("Markus"))
                .body("lastname", equalTo("Müller"));
    }

    @Test
    @Order(5)
    public void getUsersTest() {

        final Response response = when().get("/password-service/users")
                .then()
                .statusCode(200).
                        extract().response();

        assertThat(response.jsonPath().getList("$").size(), greaterThanOrEqualTo(1));
    }

    @Test
    @Order(6)
   //@Disabled
    public void deleteUsersTest() {
        with()
                .when()
                .delete("/password-service/users/{login}",LOGIN)
                .then()
                .statusCode(204);
    }

    @Test
    @Order(7)
    //@Disabled
    public void getUserAfterDeletionTest() {
        when().get("/password-service/users/" + LOGIN)
                .then()
                .statusCode(204);
    }
}