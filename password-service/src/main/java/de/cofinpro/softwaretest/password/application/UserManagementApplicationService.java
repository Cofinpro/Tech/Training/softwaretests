package de.cofinpro.softwaretest.password.application;

import de.cofinpro.softwaretest.password.application.commands.ChangeNameCommand;
import de.cofinpro.softwaretest.password.application.commands.CreateUserCommand;
import de.cofinpro.softwaretest.password.application.commands.RemoveUserCommand;
import de.cofinpro.softwaretest.password.application.exception.ApplicationBusinessException;
import de.cofinpro.softwaretest.password.application.exception.UserAlreadyExistsApplicationException;
import de.cofinpro.softwaretest.password.application.exception.UserNotFoundApplicationException;
import de.cofinpro.softwaretest.password.domain.User;
import de.cofinpro.softwaretest.password.domain.UserRepository;
import de.cofinpro.softwaretest.password.domain.exception.BusinessException;
import de.cofinpro.softwaretest.password.domain.exception.UserNotFoundException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

/**
 * @author DKostrzycki
 */
@Stateless
@LocalBean
public class UserManagementApplicationService implements UserManagementUseCase {

    @Inject
    private UserRepository userRepository;

    public Collection<User> readAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User readUser(String login) {
        try {
            return userRepository.read(login);
        } catch (UserNotFoundException e) {
            return null;
        }
    }

    @Override
    public User createUser(CreateUserCommand createUserCommand) {
        final User user = new User(createUserCommand.getLogin(),
                createUserCommand.getFirstname(),
                createUserCommand.getLastname());
        try {
            userRepository.read(createUserCommand.getLogin());
        } catch (UserNotFoundException e) {
            userRepository.save(user);
            return user;
        }
        throw new UserAlreadyExistsApplicationException("User " + createUserCommand.getLogin() + " already exists!");
    }

    @Override
    public void removeUser(RemoveUserCommand removeUserCommand) {
        final User user = readUserHelper(removeUserCommand.getLogin());
        try {
            userRepository.delete(user);
        } catch (UserNotFoundException e) {
            throw new UserNotFoundApplicationException("User " + user.getLogin() + " cannot be found", e);
        }
    }

    @Override
    public void changeName(ChangeNameCommand changeNameCommand) {
        final String login = changeNameCommand.getLogin();
        final User user = readUserHelper(login);
        try {
            user.changeFirstname(changeNameCommand.getFirstname());
            user.changeLastname(changeNameCommand.getLastname());
        } catch (BusinessException e) {
           throw new ApplicationBusinessException("Changing name failed", e);
        }

        userRepository.save(user);
    }

    private User readUserHelper(String login) {
        try {
            return userRepository.read(login);
        } catch (UserNotFoundException e) {
            throw new UserNotFoundApplicationException("User " + login + " cannot be found", e);
        }
    }
}
