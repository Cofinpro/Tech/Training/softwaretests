package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
public class RemoveUserCommand extends Command {
    public RemoveUserCommand(String login) {
        super(login);
    }
}
