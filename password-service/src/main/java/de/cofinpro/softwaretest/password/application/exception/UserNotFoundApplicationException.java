package de.cofinpro.softwaretest.password.application.exception;


/**
 * @author DKostrzycki
 */
public class UserNotFoundApplicationException extends ApplicationException {
    public UserNotFoundApplicationException(String message, Throwable cause) {
        super(message,cause);
    }
}
