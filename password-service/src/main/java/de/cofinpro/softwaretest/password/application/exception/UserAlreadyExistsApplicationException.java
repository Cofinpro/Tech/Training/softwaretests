package de.cofinpro.softwaretest.password.application.exception;


/**
 * @author DKostrzycki
 */
public class UserAlreadyExistsApplicationException extends ApplicationException {
    public UserAlreadyExistsApplicationException(String message) {
        super(message);
    }
}
