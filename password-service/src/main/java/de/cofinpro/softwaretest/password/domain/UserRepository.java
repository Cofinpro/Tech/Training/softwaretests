package de.cofinpro.softwaretest.password.domain;

import de.cofinpro.softwaretest.password.domain.exception.UserNotFoundException;

import java.util.Collection;

/**
 * @author DKostrzycki
 */
public interface UserRepository {
    User read(String login) throws UserNotFoundException;

    void delete(User user) throws UserNotFoundException;

    void save(User user);

    Collection<User> findAll();
}
