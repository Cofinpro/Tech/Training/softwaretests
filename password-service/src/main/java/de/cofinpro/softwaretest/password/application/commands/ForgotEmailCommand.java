package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
public class ForgotEmailCommand extends Command {
    public ForgotEmailCommand(String login) {
        super(login);
    }
}
