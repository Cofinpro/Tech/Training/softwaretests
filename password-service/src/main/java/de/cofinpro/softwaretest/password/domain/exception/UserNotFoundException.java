package de.cofinpro.softwaretest.password.domain.exception;

/**
 * @author DKostrzycki
 */
public class UserNotFoundException extends Exception {
    public UserNotFoundException(String message) {
        super(message);
    }
}
