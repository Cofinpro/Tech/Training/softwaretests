package de.cofinpro.softwaretest.password.controller.dto;

/**
 * @author DKostrzycki
 */
public class NameDto {

    private String firstname;
    private String lastname;

    public NameDto() {
    }

    public NameDto(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
