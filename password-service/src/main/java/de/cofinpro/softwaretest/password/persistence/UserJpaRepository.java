package de.cofinpro.softwaretest.password.persistence;

import de.cofinpro.softwaretest.password.domain.User;
import de.cofinpro.softwaretest.password.domain.UserRepository;
import de.cofinpro.softwaretest.password.domain.exception.UserNotFoundException;
import de.cofinpro.softwaretest.password.persistence.mapper.UserMapper;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author DKostrzycki
 */

@Dependent
public class UserJpaRepository implements UserRepository {

    @Inject
    private EntityManager entityManager;

    @Inject
    private UserMapper userMapper;

    @Override
    public User read(String login) throws UserNotFoundException {
        final UserEntity foundUserEntity = entityManager.find(UserEntity.class, login);
        if (foundUserEntity == null) {
            throw new UserNotFoundException(login);
        }
        return userMapper.mapToDomain(foundUserEntity);
    }

    @Override
    public void delete(User user) throws UserNotFoundException {
        final UserEntity foundUserEntity = entityManager.find(UserEntity.class, user.getLogin());
        if (foundUserEntity == null) {
            throw new UserNotFoundException(user.getLogin());
        }
        entityManager.remove(foundUserEntity);
    }

    @Override
    public void save(User user) {
        final UserEntity foundUserEntity = entityManager.find(UserEntity.class, user.getLogin());
        if (foundUserEntity == null) {
            entityManager.persist(userMapper.mapToEntity(user));
        } else {
            entityManager.merge(userMapper.mapToEntity(user));
        }
    }

    @Override
    public Collection<User> findAll() {
        final TypedQuery<UserEntity> query = entityManager.createQuery("SELECT u FROM UserEntity u", UserEntity.class);
        final List<UserEntity> resultList = query.getResultList();
        return userMapper.mapToDomain(resultList);
    }
}
