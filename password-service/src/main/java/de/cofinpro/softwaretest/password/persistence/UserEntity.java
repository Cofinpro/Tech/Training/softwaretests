package de.cofinpro.softwaretest.password.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author DKostrzycki
 */
@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    private String login;
    private String firstname;
    private String lastname;
    private String password;

    public UserEntity() {
    }

    public UserEntity(String login, String firstname, String lastname, String password) {
        this.login = login;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
