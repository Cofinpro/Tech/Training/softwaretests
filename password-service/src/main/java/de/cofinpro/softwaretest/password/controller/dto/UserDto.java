package de.cofinpro.softwaretest.password.controller.dto;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * @author DKostrzycki
 */
public class UserDto {

    @NotBlank
    private String login;
    private boolean defaultPassword;
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;

    public UserDto() {
    }

    public UserDto(String login, String firstname, String lastname, boolean defaultPassword) {
        this.login = login;
        this.firstname = firstname;
        this.lastname = lastname;
        this.defaultPassword = defaultPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(boolean defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto user = (UserDto) o;
        return login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}
