package de.cofinpro.softwaretest.password.application;

import de.cofinpro.softwaretest.password.application.commands.ChangePasswordCommand;
import de.cofinpro.softwaretest.password.application.commands.ForgotEmailCommand;
import de.cofinpro.softwaretest.password.application.commands.ForgotPasswordCommand;

/**
 * Password Management with three use cases change, forgot and forgot password
 *
 */
public interface PasswordManagementUseCase {

    void changePassword(ChangePasswordCommand changePasswordCommand);

    void forgotEmail(ForgotEmailCommand forgotEmailCommand);

    void forgotPassword(ForgotPasswordCommand forgotEmailCommand);
}
