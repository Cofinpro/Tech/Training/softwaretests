package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
public class ChangePasswordCommand extends Command {
    private final String oldPassword;
    private final String newPassword;

    public ChangePasswordCommand(String login, String oldPassword, String newPassword) {
        super(login);
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
