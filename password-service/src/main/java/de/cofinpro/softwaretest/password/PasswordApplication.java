package de.cofinpro.softwaretest.password;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author DKostrzycki
 */
@ApplicationPath("/")
public class PasswordApplication extends Application {
}

