package de.cofinpro.softwaretest.password.domain.exception;

/**
 * @author DKostrzycki
 */
public class WrongPasswordBusinessException extends BusinessException {
    public WrongPasswordBusinessException(String message) {
        super(message);
    }
}
