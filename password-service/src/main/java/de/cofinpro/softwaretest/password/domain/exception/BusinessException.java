package de.cofinpro.softwaretest.password.domain.exception;

/**
 * @author DKostrzycki
 */
public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
