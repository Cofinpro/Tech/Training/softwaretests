package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
public class ChangeNameCommand extends Command {
    private final String firstname;
    private final String lastname;

    public ChangeNameCommand(final String login, final String firstname, final String lastname) {
        super(login);
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
