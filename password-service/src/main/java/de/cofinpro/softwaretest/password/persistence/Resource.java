package de.cofinpro.softwaretest.password.persistence;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author DKostrzycki
 */
@Dependent
public class Resource {
    // Expose an entity manager using the resource producer pattern
    @PersistenceContext(unitName = "pu")
    @Produces
    private EntityManager em;
}
