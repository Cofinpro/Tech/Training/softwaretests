package de.cofinpro.softwaretest.password.application.exception;

/**
 * @author DKostrzycki
 */
public abstract class ApplicationException extends RuntimeException {
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(String message) {
        super(message);
    }
}
