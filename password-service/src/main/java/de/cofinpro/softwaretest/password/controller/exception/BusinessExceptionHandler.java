package de.cofinpro.softwaretest.password.controller.exception;

import de.cofinpro.softwaretest.password.application.exception.ApplicationBusinessException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author DKostrzycki
 */
@Provider
public class BusinessExceptionHandler implements
        ExceptionMapper<ApplicationBusinessException> {

    @Override
    public Response toResponse(ApplicationBusinessException exception) {
        return Response.status(451).entity(exception.getCause().getMessage()).build();
    }
}
