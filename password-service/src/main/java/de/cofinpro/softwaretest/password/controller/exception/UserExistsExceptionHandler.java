package de.cofinpro.softwaretest.password.controller.exception;

import de.cofinpro.softwaretest.password.application.exception.UserAlreadyExistsApplicationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author DKostrzycki
 */
@Provider
public class UserExistsExceptionHandler implements
        ExceptionMapper<UserAlreadyExistsApplicationException> {

    @Override
    public Response toResponse(UserAlreadyExistsApplicationException exception) {
        return Response.status(Response.Status.CONFLICT).entity(exception.getMessage()).build();
    }
}
