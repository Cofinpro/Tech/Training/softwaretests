package de.cofinpro.softwaretest.password.controller.exception;

import de.cofinpro.softwaretest.password.application.exception.UserNotFoundApplicationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author DKostrzycki
 */
@Provider
public class UserNotFoundExceptionHandler implements
        ExceptionMapper<UserNotFoundApplicationException> {

    @Override
    public Response toResponse(UserNotFoundApplicationException exception) {
        return Response.status(Response.Status.CONFLICT).entity(exception.getMessage()).build();
    }
}
