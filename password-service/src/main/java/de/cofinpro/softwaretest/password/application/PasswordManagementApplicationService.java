package de.cofinpro.softwaretest.password.application;

import de.cofinpro.softwaretest.password.application.commands.ChangePasswordCommand;
import de.cofinpro.softwaretest.password.application.commands.ForgotEmailCommand;
import de.cofinpro.softwaretest.password.application.commands.ForgotPasswordCommand;
import org.jboss.resteasy.spi.NotImplementedYetException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * @author DKostrzycki
 */
@Stateless
@LocalBean
public class PasswordManagementApplicationService implements PasswordManagementUseCase {

    @Override
    public void changePassword(ChangePasswordCommand changePasswordCommand) {
        //TODO 1 Implement the application logic
        throw new NotImplementedYetException("Please implement changePassword.");
    }

    @Override
    public void forgotPassword(ForgotPasswordCommand forgotEmailCommand) {
        //TODO 3 Implement the application logic
    }

    @Override
    public void forgotEmail(ForgotEmailCommand forgotEmailCommand) {
    }


}
