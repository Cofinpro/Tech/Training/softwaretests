package de.cofinpro.softwaretest.password.application;

import de.cofinpro.softwaretest.password.application.commands.ChangeNameCommand;
import de.cofinpro.softwaretest.password.application.commands.CreateUserCommand;
import de.cofinpro.softwaretest.password.application.commands.RemoveUserCommand;
import de.cofinpro.softwaretest.password.domain.User;

/**
 * The use case is to manage a user from creation to deletion
 *
 */
public interface UserManagementUseCase {
    User readUser(String login);

    User createUser(CreateUserCommand createUserCommand);

    void removeUser(RemoveUserCommand removeUserCommand);

    void changeName(ChangeNameCommand changeNameCommand);
}
