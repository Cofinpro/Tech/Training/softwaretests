package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
public class ForgotPasswordCommand extends Command {
    private String email;

    public ForgotPasswordCommand(String login, String email) {
        super(login);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
