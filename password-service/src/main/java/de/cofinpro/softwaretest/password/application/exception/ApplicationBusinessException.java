package de.cofinpro.softwaretest.password.application.exception;

import de.cofinpro.softwaretest.password.domain.exception.BusinessException;

/**
 * @author DKostrzycki
 */
public class ApplicationBusinessException extends ApplicationException {

    public ApplicationBusinessException(String message, BusinessException e) {
        super(message, e);
    }
}
