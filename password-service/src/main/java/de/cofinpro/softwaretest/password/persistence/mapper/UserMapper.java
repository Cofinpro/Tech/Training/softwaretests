package de.cofinpro.softwaretest.password.persistence.mapper;

import de.cofinpro.softwaretest.password.controller.dto.UserDto;
import de.cofinpro.softwaretest.password.domain.User;
import de.cofinpro.softwaretest.password.persistence.UserEntity;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author DKostrzycki
 */
@ApplicationScoped
public class UserMapper {
    public User mapToDomain(UserEntity user) {
        return user != null ? new User(user.getLogin(), user.getFirstname(), user.getLastname(), user.getPassword()) : null;
    }

    public Collection<User> mapToDomain(Collection<UserEntity> users) {
        return Optional.ofNullable(users).orElse(Collections.emptyList())
                .stream()
                .map(this::mapToDomain)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public UserEntity mapToEntity(User user) {
        return user != null ?
                new UserEntity(user.getLogin(), user.getFirstname(), user.getLastname(), user.getPassword()) : null;
    }

    public Collection<UserEntity> mapToEntity(Collection<User> users) {
        return Optional.ofNullable(users).orElse(Collections.emptyList())
                .stream()
                .map(this::mapToEntity)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
