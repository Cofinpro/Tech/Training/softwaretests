package de.cofinpro.softwaretest.password.application.commands;

/**
 * @author DKostrzycki
 */
abstract class Command {
    private String login;

    Command(){}

    Command(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
