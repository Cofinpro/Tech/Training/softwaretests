package de.cofinpro.softwaretest.password.controller.mapper;

import de.cofinpro.softwaretest.password.controller.dto.UserDto;
import de.cofinpro.softwaretest.password.domain.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author DKostrzycki
 */
@ApplicationScoped
public class UserMapper {
    public UserDto mapToDto(User user) {
        return user != null ? new UserDto(
                user.getLogin(),
                user.getFirstname(),
                user.getLastname(),
                user.isDefaultPassword()) : null;
    }

    public Collection<UserDto> mapToDto(Collection<User> users) {
        return Optional.ofNullable(users).orElse(Collections.emptyList())
                .stream()
                .map(this::mapToDto)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
