package de.cofinpro.softwaretest.password.controller;

import de.cofinpro.softwaretest.password.application.PasswordManagementApplicationService;
import de.cofinpro.softwaretest.password.application.UserManagementApplicationService;
import de.cofinpro.softwaretest.password.application.commands.ChangeNameCommand;
import de.cofinpro.softwaretest.password.application.commands.ChangePasswordCommand;
import de.cofinpro.softwaretest.password.application.commands.CreateUserCommand;
import de.cofinpro.softwaretest.password.application.commands.RemoveUserCommand;
import de.cofinpro.softwaretest.password.controller.dto.NameDto;
import de.cofinpro.softwaretest.password.controller.dto.PasswordDto;
import de.cofinpro.softwaretest.password.controller.dto.UserDto;
import de.cofinpro.softwaretest.password.controller.mapper.UserMapper;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

/**
 * @author DKostrzycki
 */
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    private UserManagementApplicationService userManagementApplicationService;

    @Inject
    private PasswordManagementApplicationService passwordApplicationService;

    @Inject
    private UserMapper userMapper;

    @GET
    @Produces("text/plain")
    @Path("is-running")
    public String isRunning() {
        return "ok";
    }

    @GET
    @Path("/{login}")
    public UserDto getUser(@PathParam("login") final String login) {
        return userMapper.mapToDto(userManagementApplicationService.readUser(login));
    }

    @GET
    @Path("/")
    public Collection<UserDto> getUsers() {
        return userMapper.mapToDto(userManagementApplicationService.readAllUsers());
    }

    @POST
    @Path("/")
    public String addUser(final UserDto userDto) {
        CreateUserCommand createUserCommand = new CreateUserCommand(
                userDto.getLogin(),
                userDto.getFirstname(),
                userDto.getLastname());
        return userManagementApplicationService.createUser(createUserCommand).getLogin();
    }

    @DELETE
    @Path("/{login}")
    public void removeUser(@PathParam("login") final String login) {
        final RemoveUserCommand removeUserCommand = new RemoveUserCommand(login);
        userManagementApplicationService.removeUser(removeUserCommand);
    }

    @PUT
    @Path("/{login}/name")
    public void changeName(@PathParam("login") final String login, final NameDto name) {
        final ChangeNameCommand changeNameCommand = new ChangeNameCommand(login, name.getFirstname(), name.getLastname());
        userManagementApplicationService.changeName(changeNameCommand);
    }

    @PUT
    @Path("/{login}/password")
    public void changePassword(@PathParam("login") final String login, final PasswordDto passwordDto) {
        passwordApplicationService.changePassword(
                new ChangePasswordCommand(login, passwordDto.getOldPassword(), passwordDto.getNewPassword()));
    }

}
