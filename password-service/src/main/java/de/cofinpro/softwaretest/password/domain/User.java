package de.cofinpro.softwaretest.password.domain;

import de.cofinpro.softwaretest.password.domain.exception.BusinessException;
import de.cofinpro.softwaretest.password.domain.exception.WrongPasswordBusinessException;

import java.util.Objects;

/**
 * @author DKostrzycki
 */
public class User {
    private String login;
    private String password;
    private String firstname;
    private String lastname;

    private static final String DEFAULT_PASSWORD = "Start1234";

    /**
     * This is used by the persistence layer
     *
     * @param login
     * @param firstname
     * @param lastname
     * @param password
     */
    public User(String login, String firstname, String lastname, String password) {
        this.login = login;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
    }

    /**
     * This is used by the application layer
     *
     * @param login
     * @param firstname
     * @param lastname
     */
    public User(String login, String firstname, String lastname) {
        this.login = login;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = DEFAULT_PASSWORD;
    }

    public void changeFirstname(String firstname) throws BusinessException {
        if (firstname != null && !firstname.isEmpty()) {
            this.firstname = firstname;
        } else {
            throw new BusinessException("Firstname has to be set");
        }
    }

    public void changeLastname(String lastname) throws BusinessException {
        if (lastname != null && !lastname.isEmpty()) {
            this.lastname = lastname;
        } else {
            throw new BusinessException("Lastname has to be set");
        }
    }

    public void changePassword(String oldPassword, String newPassword) throws WrongPasswordBusinessException {
        // TODO 2 implement a password validation.
        // The new password must consist of at least 8 alpha numeric and one special characters
    }

    public final boolean isDefaultPassword() {
        return DEFAULT_PASSWORD.equals(password);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstname() {
        return firstname;
    }


    public String getLastname() {
        return lastname;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}
