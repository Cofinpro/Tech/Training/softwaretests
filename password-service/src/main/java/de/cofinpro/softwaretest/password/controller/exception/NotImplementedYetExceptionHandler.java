package de.cofinpro.softwaretest.password.controller.exception;

import org.jboss.resteasy.spi.NotImplementedYetException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author DKostrzycki
 */
@Provider
public class NotImplementedYetExceptionHandler implements
        ExceptionMapper<NotImplementedYetException> {

    @Override
    public Response toResponse(NotImplementedYetException exception) {
        return Response.status(Response.Status.NOT_IMPLEMENTED).entity(exception.getMessage()).build();
    }
}
